#!/bin/bash

echo "Estrutura de pastas"
BASE="dados-covid"

mkdir $BASE

echo "nomemclatura padrão pra salvar arquivos"

FOLDER=$( date +"%Y-$m-%d" )

echo "criando pastas"

mkdir "$BASE/manual-$FOLDER"
mkdir "$BASE/dados-$FOLDER"

echo "configurando links"

LINKCODE="https://github.com/owid/covid-19-data/blob/master/public/data/owid-covid-codebook.csv"

LINKDADOS="https://github.com/owid/covid-19-data/raw/master/public/data/owid-covid-codebook.csv"

echo "configurando,baixando,salvando e movendo arquivos do covid"

NOMECODE="code-dados-covid-owid.csv"
NOMEDADOS="dados-covid-owid.csv"

wget --output-document="$BASE/code-$FOLDER/$NOMECODE" "${LINKCODE}"
wget --output-document="$BASE/dados-$FOLDER/$NOMEDADOS" "${LINKDADOS}"
