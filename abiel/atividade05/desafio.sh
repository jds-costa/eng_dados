#!/bin/bash

INPUT=dados-covid-owid.csv

echo "extraindo e ordanando países"

awk -F ',' '{print $3}' $INPUT | sort | uniq > paises-afetados-covid-owid.txt

echo "Arquivo gerado"
