
#!/bin/bash

echo "informa a quantidade pra invocar um script caso seja maior que 2"
read -p "digite um número: " numero


echo "registra o número informado e caso seja maior que 2, salva arquivos com conteúdo top pra cada número iterado"
if [[ $numero -le 2 ]];
then
  echo "não será necessário iteração"
else
 for (( x=1; x<=$numero; x++ ))
 do
 top -n $x > iteracao-${x}.log
 done
fi

echo "cria pastas de backup impar e par separadamente"
mkdir backup_impar
mkdir backup_par

for i in $(ls *.log)
do
NUM=$(echo "$i" | grep -o -E '[0-9]+')
 if [[ $(($NUM%2)) -eq '0' ]]; then
  echo "copiando arquivo iteracao-${NUM}.log para a pasta backup_par"
  cp iteracao-"${NUM}".log backup_par/
else
  echo "copiando arquivo iteracao-${NUM}.log para a pasta backup_par"
  cp iteracao-"${NUM}".log backup_impar/
fi
done

echo "Gerando tarball"
tar -cf backup_par_impar.tar backup_par/ backup_impar/
else
echo "numero de iteracao menor igual a 2"
fi


