#!/bin/bash

echo "Dados de consumo"

ps -eo pmem,cmd | sort -k 1 -nr | head -5 > texto_tmp.txt
rm -rf consumo.txt
DATE=$(date +"%Y-%m-%d")
HOUR=$(date +"%T")

while read line; do
  ARR=( $(IFS=" "; echo $line))
  MEN=${ARR[0]}
  PROCESS=${ARR[1]}
  echo "$DATE, $HOUR, $PROCESS, $MEN MB" >> consumo.txt
done < texto_tmp.txt
echo "Arquivo - consumo.txt - criado com sucesso"
rm -rf texto_temp.txt
echo "Final da execução"

