
#!/bin/bash

touch log1.txt log2.txt
echo "arquivos de texto log1 e log2 criados"

ls -lha > log1.txt
echo "conteúdo de ls -lha copiado para o arquivo log1"

top -n 1 > log2.txt
echo "conteúdo top armazenado no arquivo log2"

mkdir backup_logs
echo "pasta backup_logs criada"

cp log1.txt log2.txt backup_logs/
echo "Arquivos log1 e log2 copiados para pasta backup_logs"

echo "Atividade concluída com sucesso"

