#!/usr/bin/bash

LOG_DIR=~/processos-recurso-uso-$(date +%F)
LOG_ARQ=$LOG_DIR/processos-uso
mkdir -p $LOG_DIR

ps -eo pid,ppid,cmd,%mem,%cpu --sort=-%mem | head  >> $LOG_ARQ-$(date +%T).log

