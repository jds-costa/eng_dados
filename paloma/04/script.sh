#!/bin/bash

PS=$(ps -eo comm,rss --no-headers --sort -rss | head -n 5 | awk '{OFS=", "; print $1, $2}' | numfmt --from-unit=1024 --to=iec --field 2 --suffix=B)

data=$(date +%Y-%m-%d)
hora=$(date +%T)

while read line; do
	echo "$data, $hora, $line" >> $data.txt
done <<< $PS


