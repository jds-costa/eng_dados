#!/bin/bash

link_data="https://github.com/owid/covid-19-data/raw/master/public/data/owid-covid-data.csv"
link_manual="https://github.com/owid/covid-19-data/raw/master/public/data/owid-covid-codebook.csv"

wget -O "dados-covid-owid.csv" $link_data
wget -O "manual-dados-covid-owid.csv" $link_manual
