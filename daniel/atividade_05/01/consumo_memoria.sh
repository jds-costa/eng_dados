#!/bin/bash

echo "Adquirindo dados de consumo"

DATA=$(date +"%Y-%m-%d") 
HORA=$(date +"%T") 
HORA_TRACOS=$(date +'%H-%M-%S')

PS_FORMATADO=$(ps -eo rss,comm --sort -rss --no-headers | head -n 5 | numfmt --to=iec --from-unit=1024 --suffix=B --field 1 | awk '{OFS=", "; print $2, $1}')

#Criando pasta
PWD=$(pwd)
mkdir -p $PWD"/"$DATA

while read line; do 
  echo $DATA, $HORA, $line >> $PWD"/"$DATA"/"$DATA.txt 
done <<< $PS_FORMATADO
echo "Arquivo criado com sucesso"
echo "Final da execução"
