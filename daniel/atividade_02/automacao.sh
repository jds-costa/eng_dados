#!/bin/bash

echo "Criar arquivos de log"

touch log1.txt
touch log2.txt

echo "Inserindo informações no arquivo log1.txt"
ls -lha > log1.txt

echo "Inserindo informações no arquivo log2.txt"
top -n 1 > log2.txt

echo "Criando pasta de backup"
mkdir backups_logs

echo "Copiar arquivos de backup"
cp log1.txt log2.txt backups_logs/

echo "Fim da execução"
