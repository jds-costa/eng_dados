#!/bin/bash

echo "A quantidade informada foi de $1"

if [[ $1 -le 2 ]]; then
  echo "A quantidade informada é menor ou igual a 2, encerrando execução"
  exit 1
fi
for (( i=1; i<=$1; i++))
  do
    echo "Criando arquivo iteracao-${i}.log"
    top -n $i > iteracao-"${i}".log 
  done

echo "Criando as pastas - backup_par - e - backup_impar -"
mkdir -p backup_par
mkdir -p backup_impar

echo "Copiando arquivos para as pastas"
for file in  $(ls *.log)
do
  clean1=${file/*-/}
  num_iteracao=${clean1/.*/}
  #echo "Numero ${num_iteracao}"

  if [[ $(($num_iteracao%2)) -eq '0' ]]; then
    cp iteracao-"${num_iteracao}".log backup_par
  else
    cp iteracao-"${num_iteracao}".log backup_impar 
  fi
done
echo "Gerando o arquivo .tar"
tar -cf backup_par_impar.tar backup_par/ backup_impar/

echo "Final execução"


