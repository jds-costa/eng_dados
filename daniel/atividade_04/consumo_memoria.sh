#!/bin/bash

echo "Adquirindo dados de consumo"

DATA=$(date +"%Y-%m-%d") 
HORA=$(date +"%T") 
HORA_TRACOS=$(date +'%H-%M-%S')

PS_FORMATADO=$(ps -eo rss,comm --sort -rss --no-headers | head -n 5 | numfmt --to=iec --from-unit=1024 --suffix=B --field 1 | awk '{OFS=", "; print $2, $1}')

while read line; do 
  echo $DATA, $HORA, $line >> $DATA'_'$HORA_TRACOS.txt 
done <<< $PS_FORMATADO
echo "Arquivo criado com sucesso"
echo "Final da execução"
