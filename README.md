# Formação de Dados Encantech
## Trilha de Engenharia de Dados

Repositório criado para estudos da equipe 01 sobre temas relacionados a Engenharia de Dados.

### Conteúdos

- Git/GitLab
- Linux

### Equipe

- Amilton
- Abiel
- Daniel
- Paloma
- Tanous
- Thiago
