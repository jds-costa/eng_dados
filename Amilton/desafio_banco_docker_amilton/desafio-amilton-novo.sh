#!/bin/bash

#pega os arquivos
NOMES=$1
DATA=$2

echo "Iniciando a geracao de massa"

#utiliza o padão de codigo_funcionario comecando de 1000
COUNT=1000
#cria nome do arquivo
MASSA="massa.sql"
DATA_ADMISSAO=0

echo "INSERT INTO FUNCIONARIO (CODIGO_FUNCIONARIO, NOME_FUNCIONARIO, DATA_ADMISSAO) VALUES" > $MASSA

#paga o número total de linha do arquivo
TOTAL=$(wc -l < $NOMES)
#diminui 1 para o calcula da PA
TOTAL=$(( $TOTAL - 1 ))
#lógica de incremento do codigo_funcionario fornecido pelo problema
INCREMENTO_FUNCIONARIO=4

#variável utilizada como valor inicial da PA
INICIAL=$((COUNT))

while read nome; do
#retorna uma data aleatória do aquivo de datas
 DATA_ADMISSAO=$(cat $DATA | shuf -n 1)

 INSERT=$(echo "(nextval('FUNCIONARIO_SEQ'), '$nome', '$DATA_ADMISSAO')" )

#verifica se chegou ao final do arquivo cálculo da PA foi utilizado para encontar a ultima linha 
 if [[ $COUNT -eq $(($INICIAL + ($TOTAL) * $INCREMENTO_FUNCIONARIO )) ]]; then
  echo  $INSERT";" >> $MASSA
 else
  echo $INSERT"," >> $MASSA
 fi
 COUNT=$((COUNT + INCREMENTO_FUNCIONARIO))
done < $NOMES

#array com os ids da titulacao
ARR_TITULACAO=('1' '2' '3' '4' '5' '6')
echo "INSERT INTO PROFESSOR (CODIGO_FUNCIONARIO, MATRICULA_PROFESSOR , CODIGO_TITULACAO) VALUES" >> $MASSA

#condicao de parada para a selecao de professores metado da PA
PARADA=$(( $INICIAL + ($TOTAL/2) * $INCREMENTO_FUNCIONARIO )) 

COUNT=1000

while read nome; do
#retorna um valor aleatório do array de titulacao
 rand=$[$RANDOM % ${#ARR_TITULACAO[@]}]
 COUNT=$((COUNT + INCREMENTO_FUNCIONARIO))

#vefirica a condicao de parada
 if [[ $COUNT -le $PARADA ]]; then
   INSERT=$( echo "($COUNT, nextval('PROFESSOR_SEQ'), ${ARR_TITULACAO[rand]})") 
   if [[ $COUNT -eq $PARADA ]]; then
    echo $INSERT";" >> $MASSA
   else
    echo $INSERT"," >> $MASSA
   fi
 fi
 
done < $NOMES

echo "Geracao de massa finalizado"





