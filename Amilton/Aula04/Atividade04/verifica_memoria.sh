#!/bin/bash

TOTALMEN=grep MemTotal /proc/meminfo | awk '{print $2}' 

ps -eo pmem,cmd | sort -k 1 -nr | head -5 > texto.txt

DATE=$(date +"%Y-%m-%d")
HOUR=$(date +"%T")

while read line; do
ARR=( $(IFS="/"; echo $line))
MEN=${ARR[0]}
PROCESS=${ARR[3]}

echo "$DATE, $HOUR, $PROCESS, $MEN MB"
done < texto.txt


