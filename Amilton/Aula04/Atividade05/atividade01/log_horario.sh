#!/bin/bash

ADDRESS=/home/amilton/eng_dados/eng_dados/Amilton/Aula04/Atividade05/atividade01/

DATE=$( date +"%Y-%m-%d")
HOUR=$( date +"%T")

FOLDER="$ADDRESS$DATE"

mkdir -p "$FOLDER"

top -n 1 > "$FOLDER/$DATE-$HOUR-processes.log"
