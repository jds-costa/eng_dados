#!/bin/bash

echo "Criando estrutura de pasta para os dados da COVID-19"

GERAL="dados-covid"

mkdir -p $GERAL

echo "Configurando nome da pasta para salvar os arquivos"

FOLDER=$( date +"%Y-%m-%d" )

echo "Criando as pastas"

mkdir -p "$GERAL/manual-$FOLDER"
mkdir -p "$GERAL/dados-$FOLDER"

echo "Configurando links"

LINKMANUAL="https://github.com/owid/covid-19-data/blob/master/public/data/owid-covid-codebook.csv"

LINKDADOS="https://github.com/owid/covid-19-data/raw/master/public/data/owid-covid-data.csv"

echo "Configurando, baixando, salvando e movendo os arquivos"

NOMEMANUAL="manual-dados-covid-owid.csv"
NOMEDADOS="dados-covid-owid.csv"

wget --output-document="$GERAL/manual-$FOLDER/$NOMEMANUAL" "${LINKMANUAL}"
wget --output-document="$GERAL/dados-$FOLDER/$NOMEDADOS" "${LINKDADOS}"
