#!/bin/bash

dir="$(pwd)"

echo "Criando estrutura de pasta para os dados da COVID-19"

GERAL="dados-covid"

mkdir -p $GERAL

echo "Configurando nome da pasta para salvar os arquivos"

FOLDER=$( date +"%Y-%m-%d" )

echo "Criando as pastas"

mkdir -p "$GERAL/manual-2-$FOLDER"
mkdir -p "$GERAL/dados-2-$FOLDER"

echo "Configurando links"

LINKMASTER="https://github.com/owid/covid-19-data/archive/refs/heads/master.zip"

echo "Configurando, baixando, descompactando e removendo arquivo"

wget -q -O master.zip $LINKMASTER && unzip master.zip && rm master.zip 

echo "configurando pastas principal"

NAMEFOLDER="covid-19-data-master/public/data/"

echo "Salvando os nomes dos arquivos"

NAMEFILEDADOS="owid-covid-data.csv"
NAMEFILEMANUAL="owid-covid-codebook.csv"

NOMEMANUAL="manual-dados-covid-owid.csv"
NOMEDADOS="dados-covid-owid.csv"

echo "Configurando o diretorio master"
NOMEPASTADOWNLOAD="covid-19-data-master"

echo "Configurando o caminho dos arquivos baixados"
ADDR1="$dir/$NOMEPASTADOWNLOAD/public/data/"
ADDR2="$dir/"

echo "Movendo e renomeando os arquivos"
mv $ADDR1$NAMEFILEMANUAL "$ADDR2/$GERAL/manual-2-$FOLDER/$NOMEMANUAL"
mv $ADDR1$NAMEFILEDADOS "$ADDR2/$GERAL/dados-2-$FOLDER/$NOMEDADOS"

echo "Removendo a pasta master"
rm -r "$NOMEPASTADOWNLOAD/"

echo "Fim"
