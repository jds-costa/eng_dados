#!/bin/bash

echo "Iniciando a verificacao do tamanho do argumento"
if [[ $1 -gt 2 ]]; then
echo "Iniciando a iteracao do comando top"
 for (( c=1; c<=$1; c++))
 do
  echo "criando o arquivo iterecao-${c}.log"
  top -n $c > iteracao-"${c}".log
  #Forma mais simples para resolver a classificacao dos arquivos
  #if [[ $(($c%2)) -eq '0' ]]; then
  # mv iteracao-"${c}".log backup_par/
  #else
  # mv iteracao-"${c}".log backup_impar/
  #fi
 done

echo "Criando as pastas para salvar os arquivos de log"
mkdir -p  backup_par 
mkdir -p backup_impar


#forma mais complexa para classificar os arquivos
#Aqui foi utilizado uma quantidade maior de comando visto em sala de aula
for i in $(ls *.log)
do
NUM=$(echo "$i" | grep -o -E '[0-9]+')
 if [[ $(($NUM%2)) -eq '0' ]]; then
  echo "Copiando o arquivo iteracao-${NUM}.log para a pasta backup_par"
  cp iteracao-"${NUM}".log backup_par/
 else
  echo "Copiando o arquivo iteracao-${NUM}.log para a pasta backup_impar"
  cp iteracao-"${NUM}".log backup_impar/
 fi
done
echo "Gerando o tarball"
tar -cf backup_par_impar.tar backup_par/ backup_impar/
else
 echo "numero de iteracao menor igual a 2"
fi
