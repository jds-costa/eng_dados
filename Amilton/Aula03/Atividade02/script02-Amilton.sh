#!/bin/bash

touch log1.txt
echo "Arquivo log1 foi criado com sucesso"
touch log2.txt
echo "Arquivo log1 foi criado com sucesso"

ls -lha > log1.txt
echo "A saida do comando ls-lha foi copiado para o arquivo log1"
top -n 1 > log2.txt
echo "A saida do comando top foi copiado para o arquivo log1"

mkdir -p backup_logs
echo "A pasta backup_logs foi criada com sucesso"

cp log1.txt backup_logs/
echo "Arquivo log1 foi movido para a pasta backup_logs"
cp log2.txt backup_logs/
echo "Arquivo log2 foi movido para a pasta backup_los"

echo "Script concluiu a atividade com sucesso"
