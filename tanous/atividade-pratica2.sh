#!/bin/bash

echo "Criando arquivos log1.txt e log2.txt"
touch log1.txt log2.txt

echo "Armazenando o conteúdo do comando ls-lha dentro do log1.txt"
ls -lha > log1.txt
echo "Armazenando o conteúdo do comando top denreo do log2.txt"
top -n 1 > log2.txt
echo "Criando uma pasta chamada backup_logs"
mkdir backup_logs
cp log1.txt log2.txt backup_logs
echo "Atividade concluida com sucesso"


