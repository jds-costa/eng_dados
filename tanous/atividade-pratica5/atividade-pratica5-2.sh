#!/bin/bash

REP='https://github.com/owid/covid-19-data/archive/refs/heads/master.zip'
REP_DIR="dados-pratica-5_2"
wget $REP -O $REP_DIR
unzip $REP_DIR -d  dados-dest 

cp -v `find dados-dest/ -name "owid-covid-data.csv"` $(pwd)/dados-covid-owid.csv 
cp -v `find dados-dest/ -name "owid-covid-data.csv"` $(pwd)/manual-dados-covid-owid.csv
