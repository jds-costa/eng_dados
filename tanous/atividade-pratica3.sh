#!/bin/bash

if [ $# -ne 1 ]
then
    exit 1
fi

MIN_NUM_ITE=3
MAX_NUM_ITE=$1
if [ $1 -lt $MIN_NUM_ITE ]
then
    echo "Não sera necessário executar top Iterações < $MIN_NUM_ITE"
    exit
fi

mkdir -p -v backup_impar
mkdir -p -v backup_par
for ((i=1; i <=MAX_NUM_ITE; i++))
do
    touch iteracao-$i.log
    echo "Salvando Arquivo iteracao-$i.log"
    top -n 1 > iteracao-$i.log
    if [ $((i % 2)) -eq 0 ]
    then
	echo "Copiando Arquivo iteracao-$i.log para o diretorio  backup_par"
	cp iteracao-$i.log backup_par
    else
	echo "Copiando Arquivo iteracao-$i.log para o diretorio  backup_impar"
	cp iteracao-$i.log backup_impar
    fi
done

echo "Armazenando backup_par,backup_impar em backup_par_impar.tar"
tar cfv backup_par_impar.tar backup_par backup_impar
