#!/bin/bash

# Obtem pid e o nome do cmd dos 5 processos que estao consumindo mais memória
mapfile -t PROCESSOS < <(ps -eo pid,cmd,pmem | sort -k 3 -nr | head -5 | awk '{print $1,$2}')

DATA_ATUAL=$(date +%F)
HORA_ATUAL=$(date +%T)

for processo in "${PROCESSOS[@]}";
do
    echo "$processo"
    P_PID=$(echo "$processo" | awk '{print $1}')
    P_CMD=$(echo "$processo" | awk '{print $2}')
    # Obtem memória e converte % ->  KB -> MB
    P_MEMORIA=$(pmap $P_PID | grep total | sed -r 's/total +//g' | sed -r 's/K//' | awk '{$1=$1/1024;print $1,"MB";}')
    echo "$DATA_ATUAL,$HORA_ATUAL,$P_CMD,$P_MEMORIA" >> 5-processos-memoria-uso.log
done
